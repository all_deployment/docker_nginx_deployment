#!/bin/sh

ssh -o StrictHostKeyChecking=no -i $MASTER_SSH_KEY "${MASTER_SSH_USER}@${MASTER_HOST}" << 'ENDSSH'
  cd /app
  export $(cat .env | xargs)
  docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
  docker-compose -f docker-compose.prod.yml up -d
ENDSSH